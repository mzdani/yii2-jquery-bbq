<?php
namespace mdscomp;

use yii\web\AssetBundle;

class jQueryBbqAssets extends AssetBundle {
	public $sourcePath = '@vendor/mdscomp/yii2-jquery-bbq';

	public $js = [
		'//code.jquery.com/jquery-migrate-1.2.1.min.js',
		'assets/jquery.ba-bbq.js',
	];

	public $depends = [
		'yii\web\JqueryAsset',
	];
}
