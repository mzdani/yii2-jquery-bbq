jQuery BBQ: Back Button & Query Library - Yii2 Assets
=======================
Yii2 asset for [jQuery BBQ: Back Button & Query Library](https://github.com/cowboy/jquery-bbq) 

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require mdscomp/yii2-jquery-bbq "dev-master"
```

or add

```
"mdscomp/yii2-jquery-bbq": "*"
```

to the require section of your `composer.json` file.



Usage
-----

Register assets into your view.


```
use mdscomp\jQueryBbqAssets;

jQueryBbqAssets::register($this);
```